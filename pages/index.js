process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

import { useRouter } from 'next/router'
import { Container, Image, Row, Col, Button, ButtonGroup, ToggleButton } from 'react-bootstrap';

var json = {date:null, time:null, name:null, email:null, phone:null, sex:null, promotion:false};
var timeEnabled = false;
var timeslotEnabled = [];
const ButtonDate=['2021-07-29 00:00:00', '2021-07-30 00:00:00', '2021-07-31 00:00:00', '2021-08-01 00:00:00'];
const ButtonTime=['12:00 - 13:00', '13:00 - 14:00', '14:00 - 15:00', '15:00 - 16:00', '16:00 - 17:00', '17:00 - 18:00', '18:00 - 19:00'];
var jsonData = null;

const dateClick = (clickedDate_id) => {
  json.time = null;
  timeEnabled = true
  json.date = clickedDate_id;
  for (let i = 0; i < ButtonDate.length; i++) {
    document.getElementById(ButtonDate[i]).style.background = '#FBD03F';
    document.getElementById(ButtonDate[i]).style.color = '#58595B';
  }
  for(let j=0; j<ButtonTime.length; j++){
    document.getElementById(ButtonTime[j]).style.background='grey';
    document.getElementById(ButtonTime[j]).style.color='#58595B';
  }
  document.getElementById(clickedDate_id).style.background='#DB5150';
  document.getElementById(clickedDate_id).style.color='white';
  for (let i=0; i<jsonData.length; i++){
    if (jsonData[i+''].date === clickedDate_id && jsonData[i+''].available > 0){
      document.getElementById(jsonData[i+''].time_slot).style.background='white';
      document.getElementById(jsonData[i+''].time_slot).style.color='#58595B';
    }
  }
}

const TimeClick = (clickedTime_id) => {
  if (timeEnabled){
    for(let j=0; j<ButtonTime.length; j++){
      for (let i=0; i<jsonData.length; i++){
        if (jsonData[i+''].date === json.date && jsonData[i+''].time_slot === clickedTime_id && jsonData[i+''].available > 0){
          document.getElementById(ButtonTime[j]).style.background='transparent';
          document.getElementById(ButtonTime[j]).style.color='#58595B';
        }
      }
    }
    for (let i=0; i<jsonData.length; i++){
      if (jsonData[i+''].date === json.date && jsonData[i+''].time_slot === clickedTime_id && jsonData[i+''].available > 0){
        json.time = clickedTime_id;
        document.getElementById(clickedTime_id).style.background='#4CA4D7';
        document.getElementById(clickedTime_id).style.color='white';
      }
    }
  }
}

const radioClick = (id) => {
  switch (id) {
    case 'radioMale':
      document.getElementById('radioFemale').checked = false;
      json.sex = "Male";
      break;
    case 'radioFemale':
      document.getElementById('radioMale').checked = false;
      json.sex = "Female";
      break;
    case 'radioPromotion':
      json.promotion = true;
      break;
  }
}

function Home( {jsonRaw} ) {
  jsonData = jsonRaw;
  const router = useRouter();
  const onConfirm = () => {
    json.name = document.getElementById('name').value;
    json.email = document.getElementById('email').value;
    json.phone = document.getElementById('phone').value;
    var message1 = "請填寫"
    if (json.name == "") {
      message1 = message1 + "姓名"
    }
    if (json.email == "") {
      if (message1.length > 3) {
        message1 = message1 + "、"
      }
      message1 = message1 + "電郵"
    }
    if (json.phone == "") {
      if (message1.length > 3) {
        message1 = message1 + "、"
      }
      message1 = message1 + "電話"
    }
    if (json.sex == null) {
      if (message1.length > 3) {
        message1 = message1 + "、"
      }
      message1 = message1 + "性別"
    }
    var message2 = "請選擇"
    if (json.date == null) {
      message2 = message2 + "日期"
    }
    if (json.time == null) {
      if (message2.length > 3) {
        message2 = message2 + "、"
      }
      message2 = message2 + "時間"
    }
    var alertMessage = ""
    if (message1.length > 3) {
      alertMessage = message1 + "。"
    }
    if (message2.length > 3) {
      alertMessage = alertMessage + message2 + "。"
    }
    if (alertMessage.length > 0) {
      alert(alertMessage)
    } else {
      router.push('./submitted')
    }
    console.log(JSON.stringify(json));
  }

    // // button const
    // const radios = [
    //   { name: '29/7', value: '29/7' },
    //   { name: '30/7', value: '30/7' },
    //   { name: '31/7', value: '31/7' },
    //   { name: '1/8', value: '1/8' },
    // ];

    return (
      <>
      <title>myFamiGo</title>
      <link rel="icon" href="/public/favicon.ico" />
      <Image src="/1.jpg" style={{width:"100%"}}/>
      <br></br><br></br>
      <Container style={{margin:"0px", padding:"0px"}}>
      <Row style={{ paddingLeft: 94, paddingRight: 94}}>
      <Col>
        <Image src="/3.png" style={{width:"100%"}}></Image>
        <Image src="/4.png" style={{width:"100%"}}></Image><br></br>
        <Image src="5.png" style={{width:"100%"}}></Image>
      </Col>
      
      <Col>
      {/* <ButtonGroup toggle>
        {radios.map((radio, idx) => (
          <ToggleButton
            key={idx}
            type="radio"
            name="radio"
            variant="warning"
            onChange={() => dateClick(radio.value)}
          >
            {radio.name}
          </ToggleButton>
        ))}
      </ButtonGroup> */}
      <label style={{fontSize:'24px', lineHeight:'34.75px'}}>
        選擇攝影時段
      </label><br></br>
      <button type='button' id='2021-07-29 00:00:00' onClick={() => dateClick('2021-07-29 00:00:00')} style={{position:'absolute', width:'148px', height:'48px'}}>
        <div id='W297'>29/7</div>
      </button>
      <button type='button' id='2021-07-30 00:00:00' onClick={() => dateClick('2021-07-30 00:00:00')} style={{position:'absolute', width:'148px', height:'48px', left: "148px"}}>
        <div id='W307'>30/7</div>
      </button>
      <button type='button' id='2021-07-31 00:00:00' onClick={() => dateClick('2021-07-31 00:00:00')} style={{position:'absolute', left:'296px', width:'148px', height:'48px'}}>
        <div id='W317'>31/7</div>
      </button>
      <button type='button' id='2021-08-01 00:00:00' onClick={() => dateClick('2021-08-01 00:00:00')} style={{position:'absolute', left:'444px', width:'148px', height:'48px'}}>
        <div id='W18'>1/8</div>
      </button>
      </Col>
      </Row>
      </Container>
      {/* <p style={{position:'absolute', top:'819px', left:'759px'}}>
        <input type='text' id='name' placeholder='姓名' className='input'></input>
      </p>
      <p style={{position:'absolute', top:'871px', left:'759px'}}>
        <input type='text' id='email' placeholder='電郵' className='input'></input>
      </p>
      <p style={{position:'absolute', top:'923px', left:'759px'}}>
        <input type='text' id='phone' placeholder='電話' className='input'></input>
      </p>

      <div >

      <table style={{position:'absolute', top:'575px', left:'741px', width:'595px', height:'48px'}}>
      <tbody>
        <tr><td id='12:00 - 13:00' onClick={clickedTime_id => TimeClick('12:00 - 13:00')}>12:00-13:00</td>
          <td id='13:00 - 14:00' onClick={clickedTime_id => TimeClick('13:00 - 14:00')}>13:00-14:00</td></tr>
        <tr><td id='14:00 - 15:00' onClick={clickedTime_id => TimeClick('14:00 - 15:00')}>14:00-15:00</td>
          <td id='15:00 - 16:00' onClick={clickedTime_id => TimeClick('15:00 - 16:00')}>15:00-16:00</td></tr>
        <tr><td id='16:00 - 17:00' onClick={clickedTime_id => TimeClick('16:00 - 17:00')}>16:00-17:00</td>
          <td id='17:00 - 18:00' onClick={clickedTime_id => TimeClick('17:00 - 18:00')}>17:00-18:00</td></tr>
        <tr><td id='18:00 - 19:00' onClick={clickedTime_id => TimeClick('18:00 - 19:00')}>18:00-19:00</td></tr>
    </tbody>
      </table>
      </div>
  
      <input id='radioPromotion' type='radio' onClick={() => radioClick('radioPromotion')} style={{position:'absolute', top:'1021px', left:'767px'}}></input>
      <label style={{position:'absolute', top:'1017.5px', left:'794px', fontSize:'18px', lineHeight:'26.06px'}}>
      本人願意接收推廣訊息
      </label>
  
      <label style={{position:'absolute', top:'848px', left:'1135px', fontSize:'18px', lineHeight:'21.09px', fontFamily:'Roboto'}}>
      性別 ：
      </label>
      
      <input id='radioMale' onClick={() => radioClick('radioMale')} type='radio' style={{position:'absolute', top:'849px', left:'1210px'}}></input>
      <label style={{position:'absolute', top:'848px', left:'1235px', fontSize:'18px', lineHeight:'21.09px', fontFamily:'Roboto'}}>男</label>
  
      <input id='radioFemale' onClick={() => radioClick('radioFemale')} type='radio' style={{position:'absolute', top:'849px', left:'1275px'}}></input>
      <label style={{position:'absolute', top:'848px', left:'1300px', fontSize:'18px', lineHeight:'21.09px', fontFamily:'Roboto'}}>女</label>
  
      <button type='button' onClick={() => onConfirm()} style={{position:'absolute', top:'1079px', left:'747px', width:'585px', height:'43px', fontWeight:'bold', letterSpacing:'20px'}}>
        提交
      </button> */}
      </>
    );
}

export async function getStaticProps() {
  const res = await fetch('https://192.168.1.100:5003/timeslotlist');
  const jsonRaw = await res.json()

  return {
    props: {
      jsonRaw,
    },
  }
}


export default Home;
