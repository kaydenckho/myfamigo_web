import {isMobile} from "react-device-detect"

function Home() {
  if (!isMobile){
  return (
    <>
    <div style={{ position:'absolute', top:'0px', left:'0px'}}>
      <img src="/1.png" layout="fixed" width="1440px" height="429px"  alt="frame1"/>
    </div>
    <div style={{ position:'absolute', top:'87px', left:'755px'}}>
      <img src="/2.png" layout="fixed" width="564px" height="235px" alt="frame2"/>
    </div>
    <div style={{ position:'absolute', top:'0px', left:'1227px'}} priority="true">
      <img src="/ITElogo.png" layout="fixed" width="106.43px" height="66px" alt="ITE_logo_1"/>
    </div>
    <div style = {{position:'absolute', left: '420px', top: '489px'}}>
      <img src="/message.png" layout="fixed" width="601px" height="619px" alt="frame14"/>
    </div>
    <div style={{ position:'absolute', top:'1238px', left:'0px'}}>
      <img src="/blueline.png" layout="fixed" alt="blueline" width='1438.5px' height='2px'/>
    </div>
    <div style={{position:'absolute', top:'517px', left:'919px'}}>
      <img src="/circle.png" layout="fixed" width='70px' height='70px'/>
    </div>
    <a href="https://myfamigo.com/zh-termsandcondition/"
    style={{position:'absolute', top:'1275px', left:'99px'}}>
      使用條款
      </a>
      <a href="https://myfamigo.com/zh-privatepolicy/"
    style={{position:'absolute', top:'1275px', left:'170px'}}>
      私隱政策
      </a>
      <a href="https://myfamigo.com/zh-cookiespolicy/"
    style={{position:'absolute', top:'1275px', left:'241px'}}>
      COOKIES 政策
      </a>
      <a src='/.fb.png' href="https://facebook.com/" style={{position:'absolute', top:'1271px', left:'373px'}}>
      <img src="/fb.png" layout="fixed" width="11.06px" height="24px" alt="fb" />
      </a>
      <a src='/.ig.png' href="https://instagram.com/" style={{position:'absolute', top:'1275px', left:'415px'}}>
      <img src="/ig.png" layout="fixed" width="21.06px" height="21px" alt="ig" />
      </a>
      <p style={{position:'absolute', top:'1260px', left:'739px', fontSize:'14px', fontFamily:'Noto Sans TC', lineHeight:'20.27px'}}>
      ©2019-2021 myFamiGo Group Limited. All rights reserved. Travel Agents Licence No.: 354490
    </p>
    </>
  );}
  else{
    return(
      <>
      <head>
      <title>myFamiGo</title>
      <link rel="icon" href="/public/favicon.ico" />
      <meta name="viewport" content="user-scalable=no, width=798px" />
      </head>
      <div style={{ position:'absolute', top:'0px', left:'0px'}}>
       <img src="/1.png" layout="fixed" width="798" height="280px"  alt="frame1"/>
      </div>
      <div style={{ position:'absolute', top:'65px', left:'330px'}}>
        <img src="/2.png" layout="fixed" width="360px" height="150px" alt="frame2"/>
      </div>
      <div style={{ position:'absolute', top:'0px', left:'700px'}} >
        <img src="/ITElogo.png" layout="fixed" width="75px" height="50px" alt="ITE_logo_1"/>
      </div>
      <div style = {{position:'absolute', left:'148px', top: '359px'}}>
        <img src="/message.png" layout="fixed" width="502px" height="487.4px" alt="frame14"/>
      </div>
      <div style={{position:'absolute', top:'380px', left:'571px'}}>
        <img src="/circle.png" layout="fixed" width='60px' height='60px'/>
      </div>
      <div style={{ position:'absolute', top:'998px', left:'0px'}}>
        <img layout="fixed" src="/blueline.png" alt="blueline" width='798px' height='2px'/>
      </div>
      <a href="https://myfamigo.com/zh-termsandcondition/"
      style={{position:'absolute', top:'1038px', left:'234px'}}>
        使用條款
      </a>
      <a href="https://myfamigo.com/zh-privatepolicy/"
      style={{position:'absolute', top:'1038px', left:'304px'}}>
        私隱政策
      </a>
      <a href="https://myfamigo.com/zh-cookiespolicy/" style={{position:'absolute', top:'1038px', left:'376px', wordBreak:'keep-all'}} >
        COOKIES&nbsp;政策
      </a>
      <a src='/fb.png' href="https://facebook.com/" style={{position:'absolute', top:'1034px', left:'483px'}}>
        <img layout="fixed" src="/fb.png" width="11.06px" height="24px" alt="fb" />
      </a>
      <a src='/ig.png' href="https://instagram.com/" style={{position:'absolute', top:'1038px', left:'515px'}}>
        <img  layout="fixed" src="/ig.png" width="21.06px" height="21px" alt="ig" />
      </a>
      <p style={{position:'absolute', top:'1042px', left:'80px', fontSize:'14px', lineHeight:'20.27px', whiteSpace:'nowrap', padding:'30px'}}>
      ©2019-2021 myFamiGo Group Limited. All rights reserved. Travel Agents Licence No.: 354490
      </p>
      </>
    )
  }
}

export default Home